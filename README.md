# ARM Embedded Development Ghetto Style

There are still developers out there who like to be able to control almost every aspect of their project. OEM-provided IDE stuffed with toolchain of OEM's choice is not an usable option for us. We want to be able to choose (build) our own toolchain, standard library and our own IDE (or no IDE and simply use vim, because we can).

This project is simple hello world based on random STM Nucleo Example providing as-clean-as-possible CMake build system while being as comfortable for use as possible and also as generic as possible.

## Requirements

* cmake >= 3.0
* arm-none-eabi toolchain (binutils, GCC) installed into /opt/arm
* standard library of your choice (tested to work with newlib, other libraries might need asdjusting of --spec linker argument)


