function(add_hex_file TARGET)
	if (NOT CMAKE_OBJCOPY)
		message(FATAL_ERROR "Your toolchain does not provide identification of objcopy utility! Unable to generate hex file!")
	endif()

	add_custom_command(OUTPUT ${TARGET}.hex
		COMMAND ${CMAKE_OBJCOPY} ARGS -I elf32-littlearm $<TARGET_FILE:${TARGET}> -O ihex ${TARGET}.hex
		DEPENDS ${TARGET}
		COMMENT "Generating HEX file for ${TARGET}"
		)
	add_custom_target(${TARGET}-hex ALL
		DEPENDS ${TARGET}.hex
		)
endfunction()

function(add_bin_file TARGET)
	if (NOT CMAKE_OBJCOPY)
		message(FATAL_ERROR "Your toolchain does not provide identification of objcopy utility! Unable to generate hex file!")
	endif()

	add_custom_command(OUTPUT ${TARGET}.bin
		COMMAND ${CMAKE_OBJCOPY} ARGS -I elf32-littlearm $<TARGET_FILE:${TARGET}> -O binary ${TARGET}.bin
		DEPENDS ${TARGET}
		COMMENT "Generating BIN file for ${TARGET}"
		)
	add_custom_target(${TARGET}-bin ALL
		DEPENDS ${TARGET}.bin
		)
endfunction()
