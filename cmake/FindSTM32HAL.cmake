find_path(STM32HAL_INCLUDES
	NAMES stm32l0xx_hal.h
	PATHS
		STM32CubeL0
	PATH_SUFFIXES
		Drivers/STM32L0xx_HAL_Driver/Inc
	DOC "STM32 HAL include path"
)

if (NOT "${STM32HAL_INCLUDES}" STREQUAL "STM32HAL_INCLUDES-NOTFOUND")
	get_filename_component(STM32HAL ${STM32HAL_INCLUDES}/../../../ ABSOLUTE)
else()
	set(STM32HAL STM32HAL-NOTFOUND)
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(STM32HAL
	FOUND_VAR STM32HAL_FOUND
	REQUIRED_VARS 
		STM32HAL
	VERSION_VAR
		STM32HAL_VERSION
)

if (STM32HAL_FOUND)
	set(HAL_SUB_INCLUDES Drivers/CMSIS/Include Drivers/CMSIS/Device/ST/STM32L0xx/Include)
	if (BSP_NUCLEO_L0 IN_LIST STM32HAL_FIND_COMPONENTS)
		if (STM32HAL_FIND_REQUIRED_BSP_NUCLEO_L0)
			list(APPEND HAL_SUB_INCLUDES Drivers/BSP/STM32L0xx_Nucleo)

			set(bsp_SRCS ${STM32HAL}/Drivers/BSP/STM32L0xx_Nucleo/stm32l0xx_nucleo.c)
			add_library(bsp STATIC ${bsp_SRCS})
			set (STM32_SRC_PATH "${STM32HAL}/Drivers/STM32L0xx_HAL_Driver/Src")
			set (STM32_LIBNAME "${STM32HAL}/Drivers/STM32L0xx_HAL_Driver/Src/stm32l0xx_")
			list(APPEND STM32HAL_LIBRARIES bsp)
		else()
			list(APPEND HAL_SUB_INCLUDES_OPTIONAL Drivers/BSP/STM32L0xx_Nucleo)
		endif()
	endif()

	foreach(SUB_INCLUDE ${HAL_SUB_INCLUDES})
		if (IS_DIRECTORY "${STM32HAL}/${SUB_INCLUDE}")
			message(STATUS "Found: ${SUB_INCLUDE}")
			list(APPEND STM32HAL_INCLUDES "${STM32HAL}/${SUB_INCLUDE}")
		else()
			message(FATAL_ERROR "STM32 HAL distribution is broken! It does not contain subdirectory `${SUB_INCLUDE}`!")
		endif()
	endforeach()

	foreach(SUB_INCLUDE ${HAL_SUB_INCLUDES_OPTIONAL})
		if (IS_DIRECTORY "${STM32HAL}/${SUB_INCLUDE}")
			message(STATUS "Found: ${SUB_INCLUDE}")
			list(APPEND STM32HAL_INCLUDES "${STM32HAL}/${SUB_INCLUDE}")
		else()
			message(FATAL_ERROR "STM32 HAL distribution is broken! It does not contain subdirectory `${SUB_INCLUDE}`!")
		endif()
	endforeach()
	if (ADC IN_LIST STM32HAL_FIND_COMPONENTS)
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_adc.c")
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_adc_ex.c")
	endif()
	if (GPIO IN_LIST STM32HAL_FIND_COMPONENTS)
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_gpio.c")
	endif()
	if (TIM IN_LIST STM32HAL_FIND_COMPONENTS)
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_tim.c")
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_tim_ex.c")
	endif()
	if (DAC IN_LIST STM32HAL_FIND_COMPONENTS)
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_dac.c")
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_dac_ex.c")
	endif()
	if (DMA IN_LIST STM32HAL_FIND_COMPONENTS)
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_dma.c")
	endif()
	if (CORTEX IN_LIST STM32HAL_FIND_COMPONENTS)
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_cortex.c")
	endif()
	if (RCC IN_LIST STM32HAL_FIND_COMPONENTS)
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_rcc.c")
		list(APPEND hal_SRC "${STM32_LIBNAME}hal_rcc_ex.c")
	endif()
	# Other modules need to be added manually.
	if (ALL_COMPONENTS IN_LIST STM32HAL_FIND_COMPONENTS)
		file(GLOB hal_SRC
			LIST_DIRECTORIES FALSE
			CONFIGURE_DEPENDS
			${STM32_SRC_PATH}/*.c
		)
	endif()
	list(APPEND hal_SRC "${STM32_LIBNAME}hal.c")
	list(APPEND STM32HAL_LIBRARIES hal)

	add_library(hal STATIC ${hal_SRC})

	mark_as_advanced(
		STM32HAL_INCLUDES
		STM32HAL_LIBRARIES
		)
endif()
